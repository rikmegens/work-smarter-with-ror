class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :company
      t.string :address1
      t.string :address2
      t.string :zipcode
      t.string :town
      t.text :comment

      t.timestamps
    end
  end
end
