# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  company    :string(255)
#  address1   :string(255)
#  address2   :string(255)
#  zipcode    :string(255)
#  town       :string(255)
#  comment    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email      :string(255)
#

require 'spec_helper'

describe Contact do
	before { @contact = Contact.new(name: "Klaas Over", email: "klaar.over@mail.weg") }
	subject { @contact }

	it { should respond_to(:name) }
	it { should respond_to(:email) }
end
