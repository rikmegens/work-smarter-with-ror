require 'spec_helper'

describe "StaticPages" do

	subject { page }

	describe "home" do
		it "should have content on home page" do
			visit root_path
			should have_content('Work-smarter ontwart')
			should have_selector('title', text: full_title('Home'))
		end
	end

	describe "products" do
		it "should have content on Products page" do
			visit products_path
			should have_selector('h1', 		text: 'Onze producten')
			should have_selector('title', 	text: full_title('Producten'))
		end

	end

	describe "services" do
		it "should have content on Services page" do
			visit services_path
			should have_selector('h1', 		text: 'Over onze Services')
			should have_selector('title', 	text: full_title('Diensten'))		
		end

	end

	describe "wiki" do
		it "should have content on wiki page" do
			visit wiki_path
			should have_content('Wiki')
			should have_selector('title', text: full_title('Wiki'))	
		end

	end

	describe "about" do
		it "should have content on About page" do
			visit about_path
			should have_content('Over Work-smarter')
			should have_selector('title', text: full_title('Over ons'))	
		end
	end
end
