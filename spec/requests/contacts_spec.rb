require 'spec_helper'

describe "Contact pages" do

  subject { page }

  describe "new" do
    before { visit contact_path }

    it { should have_selector('h1',    text: 'Neem contact op') }
		it { should have_selector('title', text: full_title('Contact')) }	
  end
end
