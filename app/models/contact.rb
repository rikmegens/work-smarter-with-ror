# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  company    :string(255)
#  address1   :string(255)
#  address2   :string(255)
#  zipcode    :string(255)
#  town       :string(255)
#  comment    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email      :string(255)
#

class Contact < ActiveRecord::Base
  attr_accessible :address1, :address2, :email, :comment, :company, :name, :town, :zipcode
end
