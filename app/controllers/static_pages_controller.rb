class StaticPagesController < ApplicationController
  def home
  end

  def products
  end

  def services
  end

  def wiki
  end

  def about
  end
end
