# Website voor Work-smarter

Dit is de nieuwe website voor Work-smarter. Het bestaat vooralsnog uit alleen maar statische pagina's. 

In de volgende release komt ook de wiki functie beschikbaar. De inhoud van deze pagina's zal worden gemigreerd vanaf http://wiki.thinksmarter.nl

versie : 0.1
datum : 9 juli 2013
auteur : Rik Megens 